//----------------------------------------------------------
// SPI.c: Communication between the Master and slave 
// simultaneously. This is synchronous and driven by a 
// clock. In this case the Digital POT is our slave and the 
// master is our PIC. Only the PIC is sending important info.
// to the Digital POT, which the Digital POT is sending throw-
// away information. 
// Author: Zach Kobes
//----------------------------------------------------------
#include "SPI.h"

//----------------------------------------------------------
// Ports 
//----------------------------------------------------------
#define DATA_IN				RC4     
#define DATA_IN_TRI			TRISC4
#define DATA_OUT			RC5
#define DATA_OUT_TRI		TRISC5
#define CLOCK				RC3
#define CLOCK_TRI			TRISC3
#define SLAVE_SELECT		RA5
#define SLAVE_SELECT_TRI	TRISA5
#define CONTROL_REG			SSPCON
#define STATUS_REG			SSPSTAT
#define CHIP_SELECT_TRI		TRISD1
#define CHIP_SELECT			RD1

//----------------------------------------------------------
// Constants
//----------------------------------------------------------
#define COMMAND_BYTE		0x12
#define SET_SSPCON			0x30
#define SET_SSPSTAT			0x40

//----------------------------------------------------------
// The following are the function declarations available to
// be used by the SPI.h file only.
//----------------------------------------------------------
void SPICommunicate( unsigned char value );// unsigned BYTE byte )

//----------------------------------------------------------
// InitADC: Initialization of ADC 
//----------------------------------------------------------
void InitSPI( void )
{
	DATA_IN_TRI		 = 1;
	DATA_OUT_TRI	 = 0; 
	CLOCK_TRI		 = 0;
	SLAVE_SELECT_TRI = 0;
	CHIP_SELECT_TRI	 = 0;
	CHIP_SELECT		 = 1;
	DATA_IN			 = 0;
	DATA_OUT 		 = 0;
	CLOCK			 = 0;
	SLAVE_SELECT	 = 1;
	CONTROL_REG 	= SET_SSPCON;
	STATUS_REG 		= SET_SSPSTAT;
}

//----------------------------------------------------------
// SPICommunication: Communication between the Master and 
// slave simultaneously, however, info. from the slave is 
// being discarded by the master.
//----------------------------------------------------------
void SPICommunicate( unsigned char value )
{
	CONTROL_REG 	= SET_SSPCON;
	STATUS_REG 		= SET_SSPSTAT;
	
	// Put the slave select line to low (0)
	SLAVE_SELECT = 0;

	// Write the byte to SSPBUF, which also clears the BF bit
	SSPBUF = value; 
	
	// Wait until the BF bit is 1, indicating the transfer has completed
	while(BF == 0)
	{
	}
	
	// Put the slave select line to high (1)
	SLAVE_SELECT = 1;
}

//----------------------------------------------------------
// SetSPIResistance: The following sets the restistance
// value of the digital pot.
//----------------------------------------------------------
void SetSPIResistance( unsigned char value )
{
	CHIP_SELECT = 0;
	
	SPICommunicate( COMMAND_BYTE );
	SPICommunicate( value );

	CHIP_SELECT = 1; 
}

//void main()	//ONLY USED FOR TESTING PURPOSES
//{
	// For testing each component uncomment each section individually, and then bulid and run.
	//------------------------------------------------------------------------------------------
	// Testing for Digital POT 
	//------------------------------------------------------------------------------------------
	//Case 1
	//InitSPI();
	//SetSPIResistance(0x03);		// The following should display roughly 1.29K ohms for resistance 
	//Case 2 
	//SetSPIResistance(0x0D);		// The following should display roughly 5.19K ohms for resistance 
	//Case 3		
	//SetSPIResistance(0x1A);		// The following should display roughly 10.19K ohms for resistance 
	//Case 4
	//SetSPIResistance(0x33);		// The following should display roughly 19.78K ohms for resistance 
	//Case 5
	//SetSPIResistance(0x80);		// The following should display roughly 49.90K ohms for resistance 
//}