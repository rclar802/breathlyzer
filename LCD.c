//----------------------------------------------------------
// LCD.c: The following handles the LCD commands from the 
// pass code system. It initializes the LCD by turning the
// display on, setting the cursor off, and setting blink on.
// It also sets the cursor to parts of the display, sends 
//strings to the LCD display, and clears the display.
// Author: David Dulaney, Ryan Clark
//----------------------------------------------------------

#include "LCD.h"

//----------------------------------------------------------
// Ports 
//----------------------------------------------------------
#define LCD_PORT					PORTD
#define LCD_PORT_TRI				TRISD
#define LCD_RS						RC6  
#define LCD_RS_TRI					TRISC6
#define LCD_ENABLE					RC7	  
#define LCD_ENABLE_TRI				TRISC7

//----------------------------------------------------------
// Constants
//----------------------------------------------------------
#define HIGH_MASK					0xF0 
#define LOW_MASK					0x0F

#define ENTRY_MODE_INC_RIGHT        0x30
#define SET_4BIT_INTERFACE			0x20
#define FUNCTION_SET_4BIT_2LINES    0x28
#define CLEAR_DISPLAY               0x01
#define DISPLAY_ON_CURSOR_BLINKING	0x06
#define DISPLAY_ON					0x0D

#define DROP_CURSOR				    0xA8
#define SET_CURSOR_HOME				0x80
#define SHIFT_CURSOR				0x14

#define LOW_BIT_SHIFT				4
#define INT_LENGTH					6
#define DELAY_100US					1
#define DELAY_200US					2
#define DELAY_2MS					20
#define DELAY_5MS					50
#define DELAY_20MS					200
#define DELAY_LOOP					100

//----------------------------------------------------------
// The following are the function declarations available to
// be used by the LCD.h file only.
//----------------------------------------------------------
void LCDAttention( unsigned char byte, int delay );
void SendDataLCDInstructions( const unsigned char byte );
void SendDataToLCD( char byte );
void ChipEnable( void );
void ShiftLCDDisplay( void );

//----------------------------------------------------------
// InitLCD: Sets up the LCD by setting the font, cursor, 
// and blink.
//----------------------------------------------------------
void InitLCD( void )
{
	LCD_PORT 	 	= 0x00;	
	LCD_PORT_TRI 	= 0x00;	
	LCD_RS 			= 0; 
	LCD_RS_TRI		= 0;
	LCD_ENABLE 	 	= 0;
	LCD_ENABLE_TRI  = 0; 

    Delay(DELAY_20MS);				// Initial Delay 20ms
	LCDAttention( ENTRY_MODE_INC_RIGHT, DELAY_5MS );
	LCDAttention( ENTRY_MODE_INC_RIGHT, DELAY_200US );
	LCDAttention( ENTRY_MODE_INC_RIGHT, DELAY_200US );
	
	LCDAttention( SET_4BIT_INTERFACE, DELAY_100US );

	SendDataLCDInstructions( FUNCTION_SET_4BIT_2LINES );
	Delay(DELAY_200US);				// Delay 200us

	SendDataLCDInstructions( CLEAR_DISPLAY );
	Delay(DELAY_2MS);				// Delay 2ms

	SendDataLCDInstructions( DISPLAY_ON_CURSOR_BLINKING );
	Delay(DELAY_200US);				// Delay 200us

	SendDataLCDInstructions( DISPLAY_ON);
	Delay(DELAY_200US);	
}

//----------------------------------------------------------
// LCDAttention: Used to get the attention of the LCD
// and change to 4 bit mode.
//----------------------------------------------------------
void LCDAttention( unsigned char byte, int delay )
{
	PORTD = byte;
	ChipEnable();
	Delay( delay );
}

//----------------------------------------------------------
// ChipEnable: Pulses the chip enable.
//----------------------------------------------------------
void ChipEnable( void )
{
	LCD_ENABLE = 1; 
	asm("nop");
	asm("nop");
	asm("nop");
	LCD_ENABLE = 0;
}

//----------------------------------------------------------
// SendDataLCDInstructions: Sends a byte to the LCD to set
// the display settings.
//----------------------------------------------------------
void SendDataLCDInstructions( const unsigned char byte )
{
	LCD_PORT = (byte & HIGH_MASK); 
	ChipEnable();
	LCD_PORT = (byte & LOW_MASK) << LOW_BIT_SHIFT;
	ChipEnable();
	Delay(DELAY_100US); 
}

//----------------------------------------------------------
// Delay: Delays 100us however many times specified.
//----------------------------------------------------------
void Delay( const int delayAmount )
{
	for ( int j = 0; j < delayAmount; j++ )
	{
		for( int i = 0; i < DELAY_LOOP; i++ )
			asm("nop");
	}
}

//----------------------------------------------------------
// ClearDisplay: Clears display and returns cursor to home.
//----------------------------------------------------------
void ClearLCD( void )
{
	LCD_RS = 0;
	LCD_PORT = (CLEAR_DISPLAY & HIGH_MASK); 
	ChipEnable();
	LCD_PORT = (CLEAR_DISPLAY & LOW_MASK) << LOW_BIT_SHIFT;
	ChipEnable();
	Delay(DELAY_100US);
}

//----------------------------------------------------------
// SendStringToLCD: Sends a string to be written to the LCD 
// one char at a time.
//----------------------------------------------------------
void SendStringToLCD( const unsigned char * s )
{
	while( *s )
		SendDataToLCD( *s++);
}

//----------------------------------------------------------
// SendDataToLCD: Sends a byte to the LCD. 
//----------------------------------------------------------
void SendDataToLCD( char byte )
{
	LCD_RS = 1;
	LCD_PORT = (byte & HIGH_MASK); 
	ChipEnable();
	LCD_PORT = (byte & LOW_MASK) << LOW_BIT_SHIFT;
	ChipEnable();
	Delay(DELAY_100US);
}

//----------------------------------------------------------
// SendIntToLCD: Sends an integer to the LCD. 
//----------------------------------------------------------
void SendIntToLCD( unsigned int byte )
{
	unsigned char str[INT_LENGTH];
	sprintf( str, "%d", byte );
	SendStringToLCD(str);
}

//----------------------------------------------------------
// SetCursor: Sets the cursor of the LCD to the specified
// row and col on the display.
//----------------------------------------------------------
void SetCursor( int row, int col )
{	 
	if( row == 1 )
	{
		LCD_RS = 0; 
		LCD_PORT = (DROP_CURSOR & HIGH_MASK);
		ChipEnable();
		LCD_PORT = (DROP_CURSOR & LOW_MASK) << LOW_BIT_SHIFT;
		ChipEnable();
		Delay(1);
	}
	else 
	{
		LCD_RS = 0; 
		LCD_PORT = (SET_CURSOR_HOME & HIGH_MASK);
		ChipEnable();
		LCD_PORT = (SET_CURSOR_HOME & LOW_MASK) << LOW_BIT_SHIFT;
		ChipEnable();
		Delay(1);
	}
	for( int j = 0; j < col; j++ )
		ShiftLCDDisplay();			
}

//----------------------------------------------------------
// ShiftLCDDisplay: Shifts the LCD Display to the right 1.
//----------------------------------------------------------
void ShiftLCDDisplay( void )
{
	LCD_RS = 0;
	LCD_PORT = (SHIFT_CURSOR & HIGH_MASK); 
	ChipEnable();
	LCD_PORT = (SHIFT_CURSOR & LOW_MASK) << LOW_BIT_SHIFT;
	ChipEnable();
	Delay(DELAY_100US);
}

//void main()	//ONLY USED FOR TESTING PURPOSES
//{
	//------------------------------------------------------------------------------------------
	// Testing for LCD
	//------------------------------------------------------------------------------------------
	//InitLCD();				// Case 1 Leave uncommented for the rest of each case.
	//SetCursor(1,0);			// Case 2 
	//SendStringToLCD("Test");	// Case 3
	//SetCursor(1,0);			// Case 4
	//SendStringtoLCD("Test");	
	//SendIntToLCD((int)156);	// Case 5
	//SetCurosr(1,0);			// Case 6
	//SendIntToLCD((int)156);	
	//ClearLCD()				// Case 7
//}