//----------------------------------------------------------
// DIPSwitch: This handles the different states Lab5 is in.
// The states include displaying BAC percent with a chosen
// resistance, raw ADC counts from the gas sensor with 
// various resistances, the temperature reading as raw ADC, 
// in centigrade, and fahrenheit, an LCD test mode to 
// increment a count every 1/4 of a second, and a buzzer
// sound for a high and low frequency. 
// Author: Zach Kobes, David Dulaney, Ryan Clark
//----------------------------------------------------------

#ifndef __DIPSwitch_H
#define __DIPSwitch_H
#include "Global.h"

//----------------------------------------------------------
// The following are the function declarations 
//----------------------------------------------------------
void InitDIPSwitch( void );
void SwitchModes( void );

#endif