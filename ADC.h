//----------------------------------------------------------
// ADC: An analog-to-digital converter that converts a 
// continuous voltage to a digital number. This is used to 
// read from a gas sensor and a temperature sensor which 
// are specified on two different channels on the board.
// Author: David Dulaney
//----------------------------------------------------------

#ifndef __ADC_H
#define __ADC_H
#include "Global.h" 
//#include "LCD.h" //ONLY INCUDED IN ADC.h FOR TESTING PURPOSES

void InitADC( void );
unsigned int GetAnalogInput( unsigned char analogChannel );

#endif