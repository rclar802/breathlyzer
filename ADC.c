//----------------------------------------------------------
// ADC: An analog-to-digital converter that converts a 
// continuous voltage to a digital number. This is used to 
// read from a gas sensor and a temperature sensor which 
// are specified on two different channels on the board.
// Author: David Dulaney
//----------------------------------------------------------
#include "ADC.h"

//----------------------------------------------------------
// Ports 
//----------------------------------------------------------
#define ADC_PORT			PORTA
#define ADC_PORT_TRIS		TRISA
#define CONV_CLOCK			ADCS0
#define IN_PROGRESS			GO_nDONE

//----------------------------------------------------------
// Constants
//----------------------------------------------------------
#define ACQUISITION_TIME	30
#define ADC_REG_SHIFT		8
#define ADC_CONFIG_SHIFT	3
#define ADC_CONFIG1			0x80
#define ADC_CONFIG0			0x41

//----------------------------------------------------------
// The following are the function declarations available to
// be used by the ADC.h file only.
//----------------------------------------------------------
void Wait( void );

//----------------------------------------------------------
// InitADC: Initialization of ADC 
//----------------------------------------------------------
void InitADC( void )
{
	ADCON1 	= ADC_CONFIG1;    // This is because one of our pins is burnt out
	ADCON0 	= ADC_CONFIG0;    // 0100
}

//----------------------------------------------------------
// GetAnalogInput: Get the input from the ADC
//----------------------------------------------------------
unsigned int GetAnalogInput( unsigned char channel )
{
	ADCON0 = ADC_CONFIG0 | (channel << ADC_CONFIG_SHIFT);	// Configure A/D

	Wait();		// Wait Acquisition Time (30us)

	IN_PROGRESS = 1; 	// Start the conversion

	while(IN_PROGRESS)	// Loop until GO_nDONE goes back to 0
		;

	return (ADRESH << ADC_REG_SHIFT ) | ADRESL;	// Read the registers
}

//----------------------------------------------------------
// Wait: Acquisition time for the ADC which waits 30us
//----------------------------------------------------------
void Wait( void )
{
	for( int i = 0; i < ACQUISITION_TIME; i++ )
		asm("nop");
}

//------------------------------------------------------------------------------------------
// Testing for LCD
//------------------------------------------------------------------------------------------
//void main() { 
	//InitLCD();
	//InitADC();
	//SendStringToLCD("Raw ADC Temp ");		// Case 1 for Temperature
	//SendIntToLCD((int)GetAnalogInput(0x00));
	//SendStringToLCD("Raw ADC Gas ");	 	// Case 2 for GasSensor
	//SendIntToLCD((int)GetAnalogInput(0x03));
//}