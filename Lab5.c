//----------------------------------------------------------
// Lab5: This lab demonstrates the use of a gas sensor to
// show BAC at a number of different resistances, 
// a temperature sensor to show the temperature in 
// fahrenheit and centegrade, a buzzer to output a pitch 
// at a high and low frequency, and an LCD to display all 
// results.  
// Author: Ryan Clark, Zach Kobes, David Dulaney
//----------------------------------------------------------
#include "DIPSwitch.h"

// Osc = HS, Watchdog off, everything else off or disabled
__CONFIG(0x3F39);

//----------------------------------------------------------
// main: Initializes the Gas Sensor, Digital Pot, and LCD...
//----------------------------------------------------------
void main()
{
	InitDIPSwitch();
	while(1)
		SwitchModes();
}


//Throw Away code for testing LCD, Ditigal POT, ADC, and DIP Settings.
//void main()
//{
	// For testing each component uncomment each section individually, and then bulid and run.
	//------------------------------------------------------------------------------------------
	// Testing for Digital POT 
	//------------------------------------------------------------------------------------------
	//Case 1
	//InitSPI();
	//ReadGasSensor(RL1);		// The following should display roughly 1.29K ohms for resistance 
	//Case 2 
	//InitSPI();
	//ReadGasSensor(RL2);		// The following should display roughly 5.19K ohms for resistance 
	//Case 3
	//InitSPI();		
	//ReadGasSensor(RL3);		// The following should display roughly 10.19K ohms for resistance 
	//Case 4
	//InitSPI();
	//ReadGasSensor(RL4);		// The following should display roughly 19.78K ohms for resistance 
	//Case 5
	//InitSPI();
	//ReadGasSensor(RL5);		// The following should display roughly 49.90K ohms for resistance 
	
	//------------------------------------------------------------------------------------------
	// Testing for LCD
	//------------------------------------------------------------------------------------------
	// InitLCD();				// Case 1 Leave uncommented for the rest of each case.
	//SetCursor(1,0);			// Case 2 
	//SendStringToLCD("Test");	// Case 3
	//SetCursor(1,0);			// Case 4
	//SendStringtoLCD("Test");	
	//SendIntToLCD((int)156);	// Case 5
	//SetCurosr(1,0);			// Case 6
	//SendIntToLCD((int)156);	
	//ClearLCD()				// Case 7

	//------------------------------------------------------------------------------------------
	// Testing for LCD
	//------------------------------------------------------------------------------------------
	// InitLCD();				// Leave uncommented for each case.
	//while(1) {
	// 	switch(DIP_SETTINGS)
//		{
//			case DISPLAY_BAC:
//				ClearLCD();
//				SendStringToLCD("Display BAC (%)");
//				SetCursor(1,0);	
//				SendIntToLCD((int)0.00);
//				break;
//			case DISPLAY_ADC_RL:
//				ClearLCD();
//				SendStringToLCD("Raw ADC for RL");
//				SetCursor(1,0);	
//				SendIntToLCD(00);
//				break;
//			case DISPLAY_ADC_RL1:
//				ClearLCD();
//				SendStringToLCD("Raw ADC for RL1");
//				SetCursor(1,0);	
//				SendIntToLCD(00);
//				break;
//			case DISPLAY_ADC_RL2:
//				ClearLCD();
//				SendStringToLCD("Raw ADC for RL2");
//				SetCursor(1,0);	
//				SendIntToLCD(00);
//				break;
//			case DISPLAY_ADC_RL3:
//				ClearLCD();
//				SendStringToLCD("Raw ADC for RL3");
//				SetCursor(1,0);	
//				SendIntToLCD(00);
//				break;
//			case DISPLAY_ADC_RL4:
//				ClearLCD();
//				SendStringToLCD("Raw ADC for RL4");
//				SetCursor(1,0);	
//				SendIntToLCD(00);
//				break;
//			case DISPLAY_ADC_RL5:
//				ClearLCD();
//				SendStringToLCD("Raw ADC for RL5");
//				SetCursor(1,0);	
//				SendIntToLCD(00);
//				break;
//			case DISPLAY_F:
//				ClearLCD();
//				SendStringToLCD("Fahrenheit Temp");
//				SetCursor(1,0);	
//				SendIntToLCD(72);
//				break;
//			case DISPLAY_C:
//				ClearLCD();
//				SendStringToLCD("Celsius Temp");
//				SetCursor(1,0);	
//				SendIntToLCD(22);
//				break;
//			case DISPLAY_ADC_COUNTS:
//				ClearLCD();
//				SendStringToLCD("Raw ADC Temp");
//				SetCursor(1,0);	
//				SendIntToLCD(22);
//			case LCD_TEST_MODE:
//				ClearLCD();
//				SendStringToLCD("LCD Test Mode");
//				SetCursor(1,0);	
//				SendIntToLCD(22);
//				break;
//			case BUZZER_LOW_FREQ:
//				ClearLCD();
//				SendStringToLCD("Low Frequency");
//				break;
//			case BUZZER_HIGH_FREQ:
//				ClearLCD();
//				SendStringToLCD("High Frequency");
//				break;
//			default:
//				ClearLCD();
//		}
//	}	
//}