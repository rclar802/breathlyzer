//----------------------------------------------------------
// PWM.h: The following handles a simple way to generate an 
// audio signal to play on to a speaker using a hardware PWM
// output, which generates rudimentary sounds of both a high 
// and low frequencies. By the controlling the PWM we can 
// make specific frequencies with the speaker.
// Author: Ryan Clark
//----------------------------------------------------------
#ifndef __PWM_H
#define __PWM_H
#include "Global.h"

//----------------------------------------------------------
// The following are the function declarations available to
// be used by any file which includes the PWM.h
//----------------------------------------------------------
void InitPWM( void );
void HighFrequency( void );
void LowFrequency ( void );
void StopFrequency( void );

#endif