//----------------------------------------------------------
// PWM.c: The following handles a simple way to generate an 
// audio signal to play on to a speaker using a hardware PWM
// output, which generates rudimentary sounds of both a high 
// and low frequencies. By the controlling the PWM we can 
// make specific frequencies with the speaker.
// Author: Ryan Clark
//----------------------------------------------------------
#include "PWM.h"

//----------------------------------------------------------
// Ports 
//----------------------------------------------------------
#define PWM_HIG						CCPR1L
#define PWM_LOW						CCP1CON	  
#define CHIP_SELECT					RD1
#define BUZZER_PIN					RC2
#define BUZZER_PIN_TRI				TRISC2

//----------------------------------------------------------
// Constants
//----------------------------------------------------------
#define TMR2_INIT					0x10
#define ENABLE_PWM					0x07
#define PWM_HGH_PERIOD				0x3A
#define PWM_LOW_PERIOD				0xED
#define PWM_ON						0x0C

//----------------------------------------------------------
// InitDigitalPot: Sets up the Digital POT
//----------------------------------------------------------
void InitPWM( void )
{
	BUZZER_PIN_TRI	= 0; 
	TMR2 = TMR2_INIT;			// TMR2 prescale value set to 16
	T2CON = ENABLE_PWM;           // Enable PWM by writing to the T2CON register
}

//----------------------------------------------------------
// HighFrequency: Sets up a high frequency for high C.
//----------------------------------------------------------
void HighFrequency( void )
{
	PR2 = PWM_HGH_PERIOD;	// Set PWM period
	CCPR1L = PR2 >> 1;  	// Set the duty cycle
	PWM_LOW = PWM_ON;  		// Set PWM mode on 
}

//----------------------------------------------------------
// LowFrequency: Sets up a low frequency for a low C.
//----------------------------------------------------------
void LowFrequency( void )
{
	PR2 = PWM_LOW_PERIOD;	// Set PWM period
	CCPR1L = PR2 >> 1;  	// Set the duty cycle
	PWM_LOW = PWM_ON;  		// Set PWM mode on
}

//----------------------------------------------------------
// StopFrequency: Sets up a stop frequency which stops the 
// the speaker from producing a noise.
//----------------------------------------------------------
void StopFrequency( void )
{
	PR2 = 0x00;				// Set PWM period
	CCPR1L = PR2 >> 1;  	// Set the duty cycle
	PWM_LOW = PWM_ON;  		// Set PWM mode on
}
