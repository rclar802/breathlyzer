//----------------------------------------------------------
// LCD.h: The following handles the LCD commands from the 
// pass code system. It initializes the LCD by turning the
// display on, setting the cursor off, and setting blink on.
// It also sets the cursor to parts of the display, sends 
// strings to the LCD display, and clears the display.
// Author: David Dulaney, Ryan Clark
//----------------------------------------------------------

#ifndef __LCD_H
#define __LCD_H
#include "Global.h"
#include "LCD.h"

//----------------------------------------------------------
// The following are the function declarations available to
// be used by any file which includes the LCD.h
//----------------------------------------------------------
void InitLCD( void );
void ClearLCD( void );
void SendStringToLCD( const unsigned char * s );
void SendIntToLCD( unsigned int byte );
void SetCursor( int row, int col );
void Delay( const int delayAmount );

#endif