//----------------------------------------------------------
// SPI.h: Communication between the Master and slave 
// simultaneously. This is synchronous and driven by a 
// clock. In this case the Digital POT is our slave and the 
// master is our PIC. Only the PIC is sending important info.
// to the Digital POT, which the Digital POT is sending throw-
// away information. 
// Author: Zach Kobes
//----------------------------------------------------------
#ifndef __SPI_H
#define __SPI_H
#include "Global.h" 

//----------------------------------------------------------
// The following are the function declarations available to
// be used by any file which includes the SPI.h
//----------------------------------------------------------
void InitSPI( void );
void SetSPIResistance( unsigned char value );

#endif
