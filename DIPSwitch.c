//----------------------------------------------------------
// DIPSwitch: This handles the different states Lab5 is in.
// The states include displaying BAC percent with a chosen
// resistance, raw ADC counts from the gas sensor with 
// various resistances, the temperature reading as raw ADC, 
// in centigrade, and fahrenheit, an LCD test mode to 
// increment a count every 1/4 of a second, and a buzzer
// sound for a high and low frequency. 
// Author: Zach Kobes, David Dulaney, Ryan Clark
//----------------------------------------------------------

#include "DIPSwitch.h"
#include "LCD.h"
#include "SPI.h"
#include "PWM.h"
#include "ADC.h"

//----------------------------------------------------------
// Ports 
//----------------------------------------------------------
#define DIP_SWITCH_TRI				TRISB
#define DIP_SWITCH					PORTB
#define WEAK_PULLUPS				nRBPU

//----------------------------------------------------------
// Constants 
//----------------------------------------------------------
#define DIP_SWITCH_TRI_SET			0x3F
#define GAS_CHANNEL					0x03
#define TEMP_CHANNEL				0x00
#define	DISPLAY_BAC					0x00
#define DISPLAY_ADC_RL				0x20
#define DISPLAY_ADC_RL1				0x21
#define DISPLAY_ADC_RL2				0x22
#define DISPLAY_ADC_RL3				0x23
#define DISPLAY_ADC_RL4				0x24
#define DISPLAY_ADC_RL5				0x25
#define DISPLAY_F					0x30
#define DISPLAY_C					0x31
#define DISPLAY_ADC_COUNTS			0x32
#define LCD_TEST_MODE				0x38
#define BUZZER_LOW_FREQ				0x39
#define BUZZER_HIGH_FREQ			0x3A
#define RL1							0x03	//~1K
#define RL2							0x0D	//~5K
#define RL3							0x1A	//~10K
#define RL4							0x33	//~20K
#define RL5							0x80	//~50K
#define CHOOSEN_RL					RL4
#define ADC_BIT_RANGE				1024.0  //Bit range for ADC
#define SMOOTH_TEMP_VAL				0.5
#define SMOOTH_GAS_VAL				0.75
#define C_CONF_F_DEN				256.0
#define MAX_MILLIVOLTS				5000
#define MILLIVOLTS_PER_ADC			10
#define INIT_C_CONF_F				32
#define C_CONF_F_NUM				225
#define DELAY_VAL					250		//Delay 1/4sec
#define AVG_ADC_VAL					20
#define BAC_COUNTS_INTERVAL			10	
#define START_LOW_FREQ				60
#define START_HIGH_FREQ				75
#define TENS_POSITION				10

//----------------------------------------------------------
// Historical Data - data that needs to be saved globally
//----------------------------------------------------------
float rawData = 0;
float adcData = 0;
float smoothedfTemp = 0;
float smoothedcTemp = 0;
float smoothedADC = 0;
int bacIndex = 0;
unsigned int count = 0;
unsigned int initCountFlag = 1;
unsigned int avgFFlag = 0;
unsigned int avgCFlag = 0;
unsigned int avgADCFlag = 0;
unsigned int avgGasFlag = 0;
unsigned char resistance = 0;

const int MAX_BAC_INDEX = 10;
const unsigned int BAC_ADC_COUNTS[] =
{
	817, 830, 843, 856, 869, 882, 895, 908, 921, 934, 947
};

//----------------------------------------------------------
// The following are the function declarations available to
// be used by the DIPSwitch.h file only.
//----------------------------------------------------------
void ReadGasSensor( unsigned char rlVal ); 
const unsigned char* ResistanceSetting( unsigned char rlVal );
void PlayHighFrequency( void );
void PlayLowFrequency( void );
void LCDTestMode( void );
void DisplayFTemp( void );
void DisplayCTemp( void );
void DisplayRawADCTemp( void );
float AvgADC( unsigned char channel );
void DisplayBACLevel( unsigned char rlVal );
void DisplayRawADC( unsigned char rlVal );

//----------------------------------------------------------
// InitDIPSwitch: Intialize the DIP Switch.
//----------------------------------------------------------
void InitDIPSwitch( void )
{
	DIP_SWITCH_TRI = DIP_SWITCH_TRI_SET;
	DIP_SWITCH = DISPLAY_BAC;
	WEAK_PULLUPS = 0;

	InitLCD();
	InitSPI();
	InitPWM();
	InitADC();
}

//----------------------------------------------------------
// SwitchModes: This determines the state of the DIPSwitch.
//----------------------------------------------------------
void SwitchModes( void )
{
	Delay(DELAY_VAL);	
	switch(DIP_SWITCH)
	{
		case DISPLAY_BAC:
			DisplayBACLevel(CHOOSEN_RL);
			break;
		case DISPLAY_ADC_RL:
			DisplayRawADC(CHOOSEN_RL);
			break;
		case DISPLAY_ADC_RL1:
			ReadGasSensor(RL1);
			break;
		case DISPLAY_ADC_RL2:
			ReadGasSensor(RL2);
			break;
		case DISPLAY_ADC_RL3:
			ReadGasSensor(RL3);
			break;
		case DISPLAY_ADC_RL4:
			ReadGasSensor(RL4);
			break;
		case DISPLAY_ADC_RL5:
			ReadGasSensor(RL5);
			break;
		case DISPLAY_F:
			DisplayFTemp();
			break;
		case DISPLAY_C:
			DisplayCTemp();
			break;
		case DISPLAY_ADC_COUNTS:
			DisplayRawADCTemp();
			break;
		case LCD_TEST_MODE:
			LCDTestMode();
			break;
		case BUZZER_LOW_FREQ:
			PlayLowFrequency();
			break;
		case BUZZER_HIGH_FREQ:
			PlayHighFrequency();
			break;
		default:
			StopFrequency();
			ClearLCD();
			initCountFlag = 1;
	}
}

//----------------------------------------------------------
// DisplayBACLevel: This takes the raw ADC data from the 
// gas sensor and displays it as a BAC. It displays a max
// of 0.100 and a min of 0.001.
//----------------------------------------------------------
void DisplayBACLevel( unsigned char rlVal )
{
	float bacValue = 0;

	ClearLCD();
	SendStringToLCD("Display BAC (%)");
	SetCursor(1, 0);
	SetSPIResistance(rlVal);
	initCountFlag = 1;

	adcData = SMOOTH_GAS_VAL * adcData + (1 - SMOOTH_GAS_VAL) * GetAnalogInput( GAS_CHANNEL );

	if (adcData >= BAC_ADC_COUNTS[bacIndex] && adcData <= BAC_ADC_COUNTS[MAX_BAC_INDEX])	//increases or decreases bacIndex based on if adcData
		bacIndex++;																			//is in the correct range of values
	else if (bacIndex != 0 && adcData <= BAC_ADC_COUNTS[bacIndex])
		bacIndex--;
	else if (adcData >= BAC_ADC_COUNTS[MAX_BAC_INDEX])
	{
		SendStringToLCD("0.100");
		return;
	}
	
	if(bacIndex == 0)
		SendStringToLCD("0.000");	
	else
	{
		bacValue = (((bacIndex - 1) * BAC_COUNTS_INTERVAL) + ((adcData - BAC_ADC_COUNTS[bacIndex - 1]) //Determines BAC value from RL4
					* ((float)BAC_COUNTS_INTERVAL / (float)(BAC_ADC_COUNTS[bacIndex] - BAC_ADC_COUNTS[bacIndex - 1]))));
		if (bacValue >= TENS_POSITION)
			SendStringToLCD("0.0");
		else
			SendStringToLCD("0.00");
		SendIntToLCD((int)bacValue);
	}

	if (bacValue >= START_LOW_FREQ && bacValue <= START_HIGH_FREQ) //Determine if BAC values are to high play sound
		LowFrequency();
	else if (bacValue > START_HIGH_FREQ)
		HighFrequency();
	else
		StopFrequency();	
}

//----------------------------------------------------------
// ReadGasSensor: This reads the raw data from the ADC and 
// displays it to the LCD after being smoothed. 
//----------------------------------------------------------
void ReadGasSensor( unsigned char rlVal)
{
	StopFrequency(); 	//Reset any previous changes with pin modes
	initCountFlag = 1;
	ClearLCD();

	SetSPIResistance(rlVal);

	if (avgGasFlag == 0 && resistance != rlVal)
	{
		//Find initial ADC values beforehand to eliminate first initial spike in gas readings.
		rawData = AvgADC(GAS_CHANNEL);
		avgGasFlag = 1;
	}
	else
		resistance = rlVal;

	char str[] = "Raw ADC for ";
	strcat(str, ResistanceSetting(rlVal));
	SendStringToLCD(str);

	rawData = SMOOTH_GAS_VAL * rawData + (1 - SMOOTH_GAS_VAL) * GetAnalogInput(GAS_CHANNEL); //Smooth gas values
	SetCursor(1, 0);
	SendIntToLCD((int)rawData); //Display gas values
}

//----------------------------------------------------------
// ResistanceSetting: This determines which resistance 
// state to be displayed to the LCD.
//----------------------------------------------------------
const unsigned char* ResistanceSetting( unsigned char rlVal )
{
	if(rlVal == RL1)	
		return "RL1";
	else if(rlVal == RL2)
		return "RL2";
	else if(rlVal == RL3)
		return "RL3";
	else if(rlVal == RL4)
		return "RL4";
	else if(rlVal == RL5)
		return "RL5";
	else
		return "RL";
}

//----------------------------------------------------------
// PlayHighFrequency: This plays a high frequency on the 
// buzzer and displays the frequency of 1047Hz which is 
// High C octave 6.
//----------------------------------------------------------
void PlayHighFrequency( void )
{
	ClearLCD();		//Reset any previous changes with pin modes
	initCountFlag = 1;

	SendStringToLCD("High Frequency");
	SetCursor(1, 0);
	SendStringToLCD("1047Hz");
	HighFrequency();	
}

//----------------------------------------------------------
// PlayLowFrequency: This plays a low frequency on the 
// buzzer and displays the frequency of 262Hz which is 
// Middle C octave 4.
//----------------------------------------------------------
void PlayLowFrequency( void ) 
{
	ClearLCD();		//Reset any previous changes to pin modes
	initCountFlag = 1;

	SendStringToLCD("Low Frequency");
	SetCursor(1, 0);
	SendStringToLCD("262Hz");
	LowFrequency();		
}

//----------------------------------------------------------
// LCDTestMode: The LCDTestMode increments a count every 
// 1/4 of a second and displays it to the LCD.
//----------------------------------------------------------
void LCDTestMode( void ) 
{	
	if (initCountFlag == 1) //Check to see if pins have changed.
		count = 0;
	initCountFlag = 0;
	StopFrequency();	//Reset any previous changes with pin modes
	ClearLCD();
	SendStringToLCD("LCD Test Mode");
	SetCursor(1,0);
	SendIntToLCD(count++); //Increment LCD count for test mode
}

//----------------------------------------------------------
// DisplayFTemp: This displays the ADC data from temperature
// sensor and displays it in Fahrenhehit on the LCD.
//----------------------------------------------------------
void DisplayFTemp( void )
{
	StopFrequency();	//Reset any previous changes with pin modes
	ClearLCD();
	initCountFlag = 1;

	SendStringToLCD("Fahrenheit Temp");
	float fTemp;

	if (avgFFlag == 0)
	{
		//Find initial ADC values beforehand to eliminate first initial spike in temps.
		smoothedfTemp = ((AvgADC(TEMP_CHANNEL) / C_CONF_F_DEN) * C_CONF_F_NUM) + INIT_C_CONF_F;
		avgFFlag = 1;
	}

	fTemp = ((GetAnalogInput(TEMP_CHANNEL) / C_CONF_F_DEN) * C_CONF_F_NUM) + INIT_C_CONF_F; //Smooth F temp. values
	smoothedfTemp = SMOOTH_TEMP_VAL * smoothedfTemp + SMOOTH_TEMP_VAL * fTemp;
	SetCursor(1, 0);
	SendIntToLCD((int)smoothedfTemp); //Display smoothed F temp. values
}

//----------------------------------------------------------
// AvgADC: This takes an average raw ADC readings.
//----------------------------------------------------------
float AvgADC ( unsigned char channel )
{
	float result = 0;

	for(int i = 0; i < AVG_ADC_VAL; i++)
	{
		result += GetAnalogInput(channel);
	}
	return result / AVG_ADC_VAL;
}

//----------------------------------------------------------
// DisplayCTemp: This displays the ADC data from temperature
// sensor and displays it in Centigrade on the LCD.
//----------------------------------------------------------
void DisplayCTemp( void )
{
	StopFrequency();	//Reset any previous changes with pin modes
	ClearLCD();
	initCountFlag = 1;

	SendStringToLCD("Centigrade Temp");
	float cTemp;
	
	if (avgCFlag == 0)
	{
		//Find initial ADC values beforehand to eliminate first initial spike in temps.
		smoothedcTemp = (AvgADC(TEMP_CHANNEL) / ADC_BIT_RANGE) * (MAX_MILLIVOLTS / MILLIVOLTS_PER_ADC);
		avgCFlag = 1;
	}

	cTemp = ((GetAnalogInput(TEMP_CHANNEL) / ADC_BIT_RANGE) * MAX_MILLIVOLTS) / TENS_POSITION;
	smoothedcTemp = SMOOTH_TEMP_VAL * smoothedcTemp + SMOOTH_TEMP_VAL * cTemp; //Smooth C temp. values
	SetCursor(1, 0);
	SendIntToLCD((int)smoothedcTemp); //Display smoothed C temp. values
}

//----------------------------------------------------------
// DisplayRawADCTemp: This displays the raw ADC data from
// the temperature sensor and displays it to the LCD.
//----------------------------------------------------------
void DisplayRawADCTemp( void )
{
	StopFrequency();	//Reset any previous changes with pin modes
	ClearLCD();
	initCountFlag = 1;

	SendStringToLCD("Raw ADC Temp");

	if (avgADCFlag == 0)					//Find initial ADC values beforehand to eliminate first initial spike in temps.
	{
		smoothedADC = AvgADC(TEMP_CHANNEL);
		avgADCFlag = 1;
	}

	smoothedADC = SMOOTH_TEMP_VAL * smoothedADC + SMOOTH_TEMP_VAL * GetAnalogInput( TEMP_CHANNEL ); //Smooth ADC temp. values
	SetCursor(1, 0);
	SendIntToLCD((int)smoothedADC);		//Display smoothed ADC temp. values
}

//----------------------------------------------------------
// DisplayRawRL: This displays the raw ADC data from the gas 
// sensor with the choosen RL and displays it to the LCD.
//----------------------------------------------------------
void DisplayRawADC( unsigned char rlVal )
{
	StopFrequency();	//Reset any previous changes with pin modes
	ClearLCD();
	initCountFlag = 1;

	SetSPIResistance(rlVal);
	SendStringToLCD("Raw ADC for RL");

	adcData = SMOOTH_GAS_VAL * adcData + (1 - SMOOTH_GAS_VAL) * GetAnalogInput( GAS_CHANNEL ); //Smooth Raw ADC values
	SetCursor(1, 0);
	SendIntToLCD((int)adcData);		//Display smoothed raw ADC values
}



//------------------------------------------------------------------------------------------
// Testing for LCD
//------------------------------------------------------------------------------------------
//void(main) {
// InitLCD();				// Leave uncommented for each case.
//while(1) {
// 	switch(DIP_SETTINGS)
//		{
//			case DISPLAY_BAC:
//				ClearLCD();
//				SendStringToLCD("Display BAC (%)");
//				SetCursor(1,0);	
//				SendIntToLCD((int)0.00);
//				break;
//			case DISPLAY_ADC_RL:
//				ClearLCD();
//				SendStringToLCD("Raw ADC for RL");
//				SetCursor(1,0);	
//				SendIntToLCD(00);
//				break;
//			case DISPLAY_ADC_RL1:
//				ClearLCD();
//				SendStringToLCD("Raw ADC for RL1");
//				SetCursor(1,0);	
//				SendIntToLCD(00);
//				break;
//			case DISPLAY_ADC_RL2:
//				ClearLCD();
//				SendStringToLCD("Raw ADC for RL2");
//				SetCursor(1,0);	
//				SendIntToLCD(00);
//				break;
//			case DISPLAY_ADC_RL3:
//				ClearLCD();
//				SendStringToLCD("Raw ADC for RL3");
//				SetCursor(1,0);	
//				SendIntToLCD(00);
//				break;
//			case DISPLAY_ADC_RL4:
//				ClearLCD();
//				SendStringToLCD("Raw ADC for RL4");
//				SetCursor(1,0);	
//				SendIntToLCD(00);
//				break;
//			case DISPLAY_ADC_RL5:
//				ClearLCD();
//				SendStringToLCD("Raw ADC for RL5");
//				SetCursor(1,0);	
//				SendIntToLCD(00);
//				break;
//			case DISPLAY_F:
//				ClearLCD();
//				SendStringToLCD("Fahrenheit Temp");
//				SetCursor(1,0);	
//				SendIntToLCD(72);
//				break;
//			case DISPLAY_C:
//				ClearLCD();
//				SendStringToLCD("Celsius Temp");
//				SetCursor(1,0);	
//				SendIntToLCD(22);
//				break;
//			case DISPLAY_ADC_COUNTS:
//				ClearLCD();
//				SendStringToLCD("Raw ADC Temp");
//				SetCursor(1,0);	
//				SendIntToLCD(22);
//			case LCD_TEST_MODE:
//				ClearLCD();
//				SendStringToLCD("LCD Test Mode");
//				SetCursor(1,0);	
//				SendIntToLCD(22);
//				break;
//			case BUZZER_LOW_FREQ:
//				ClearLCD();
//				SendStringToLCD("Low Frequency");
//				break;
//			case BUZZER_HIGH_FREQ:
//				ClearLCD();
//				SendStringToLCD("High Frequency");
//				break;
//			default:
//				ClearLCD();
//		}
//	}	
//}